import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: UsersService;
  let usersRepo = {};
  let authService: AuthService;
  // let Repo =Repository <User >

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        AuthService,
        { provide: getRepositoryToken(User), useValue: usersRepo },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    usersRepo = module.get(getRepositoryToken(User));
    controller = module.get<UsersController>(UsersController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAllUsers should return a list of users with the given email', async () => {
    jest
      .spyOn(usersService, 'find')
      .mockResolvedValueOnce([
        { id: 1, email: 'test@gmail.com', password: 'aswre' } as User,
      ]);

    const users = await controller.findAllUsers('test@gmail.com');

    expect(usersService.find).toHaveBeenCalledTimes(1);
    expect(users.length).toBe(1);
    expect(users[0].email).toBe('test@gmail.com');
  });

  it('findUser should return a single user with the given id', async () => {
    jest.spyOn(usersService, 'findOne').mockResolvedValueOnce({
      id: 1,
      email: 'test@gmail.com',
      password: 'aswre',
    } as User);

    const user = await controller.findUser('1');

    expect(user).toBeDefined();
  });

  it('findUser should throws an error if user with given id is not found', async () => {
    jest.spyOn(usersService, 'findOne').mockReturnValue(null);

    await expect(controller.findUser(null)).rejects.toThrow(NotFoundException);
  });

  it('signIn should update session object and return user', async () => {
    const session = { userId: null };
    jest.spyOn(authService, 'signin').mockResolvedValueOnce({
      id: 1,
      email: 'test@gmail.com',
      password: 'aswre',
    } as User);

    const user = await controller.signin(
      { email: 'qewrg@wet.com', password: 'wqer' },
      session,
    );

    expect(user.id).toBe(1);
    expect(session.userId).toBe(1);
  });
});
