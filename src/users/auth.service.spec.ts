import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let service: AuthService;
  let fakeUsersService: Partial<UsersService>;

  beforeEach(async () => {
    // Create a fake copy of the users service
    const users: User[] = [];

    fakeUsersService = {
      find: (email: string) => {
        // Promise.resolve([])
        const filteredUsers = users.filter((user) => user.email === email);
        return Promise.resolve(filteredUsers);
      },
      create: (email: string, password: string) => {
        const user = {
          id: Math.floor(Math.random() * 99999),
          email,
          password,
        } as User;

        users.push(user);
        return Promise.resolve(user);
      },
    };

    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUsersService,
        },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('should create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });

  it('should create a new user with a salted and hashed password', async () => {
    const userData = {
      email: 'test@gmail.com',
      password: 'asdf',
    };

    const user = await service.signup(userData.email, userData.password);
    const [salt, hash] = user.password.split('.');

    expect(user.password).not.toBe(userData.password);
    expect(salt).toBeDefined();
    expect(hash).toBeDefined();
  });

  it('should throw an error if user signs up with email that is in use', async () => {
    expect.assertions(1);

    await service.signup('test@gmail.com', 'asdf');

    await expect(service.signup('test@gmail.com', 'asdf')).rejects.toThrow(
      BadRequestException,
    );
  });

  it('should throw an error if signin is called with an unused email', async () => {
    await expect(service.signin('test@gmail.com', 'pass')).rejects.toThrow(
      NotFoundException,
    );
  });

  it('should throw an error if an invalid password is provided', async () => {
    await service.signup('test@gmail.com', 'pass2');

    await expect(service.signin('test@gmail.com', 'pass')).rejects.toThrow(
      BadRequestException,
    );
  });

  it('should return a user if correct password is provided', async () => {
    await service.signup('test@gmail.com', 'pass');

    const user = await service.signin('test@gmail.com', 'pass');

    expect(user).toBeDefined();
  });
});
